import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    articles: []
  },
  getters: {
    sliderArticles: state => {
      return state.articles.filter(post => post.slider)
    },
    getArticleById: (state) => (id) => {
      return state.articles.find(post => post.id === +id)
    }
  },
  mutations: {
    setArticles: (state, articles) => {
      state.articles = articles
    }
  },
  actions: {
    getArticles({commit}) {
      return fetch('/articles.json')
          .then(response => response.json())
          .then(articles => commit('setArticles', articles))
    }
  },
  modules: {
  }
})
